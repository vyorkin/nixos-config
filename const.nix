{
  timeZone = "Europe/Moscow";

  theme = {
    # Use dark color theme
    dark = false;
  };
}
