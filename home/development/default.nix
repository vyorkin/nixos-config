{
  imports = [
    ./clojure.nix
    ./haskell.nix
    ./agda.nix
    ./ocaml.nix
    ./javascript.nix
    ./ruby.nix
    ./rust.nix
  ];
}
