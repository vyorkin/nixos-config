let g:seiya_target_groups = has('nvim') ? ['guibg'] : ['ctermbg']

" uncomment if your're using transperent terminal windows
let g:seiya_auto_enable = 1
