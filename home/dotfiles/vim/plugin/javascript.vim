let g:syntax_js=['function', 'semicolon', 'comma', 'return', 'this', 'proto', 'solarized', 'debug']

let g:javascript_conceal = 0
let g:javascript_enable_domhtml_css = 1
let g:javascript_fold = 0

let g:javascript_plugin_flow = 1
let g:javascript_plugin_jsdoc = 1

" othree/javascript-libraries-syntax.vim 

let g:used_javascript_libs = 'jquery,underscore,underscore,backbone,prelude,angularjs,angularui,angularuirouter,react,flux,requirejs,sugar,jasmine,chai,handlebars,ramda,vue,d3,tape'

