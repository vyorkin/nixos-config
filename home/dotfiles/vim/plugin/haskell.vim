" neovimhaskell/haskell-vim

let g:haskell_enable_quantification = 1   " enable highlighting of `forall`
let g:haskell_enable_recursivedo = 1      " enable highlighting of `mdo` and `rec`
let g:haskell_enable_arrowsyntax = 1      " enable highlighting of `proc`
let g:haskell_enable_pattern_synonyms = 1 " enable highlighting of `pattern`
let g:haskell_enable_typeroles = 1        " enable highlighting of type roles
let g:haskell_enable_static_pointers = 1  " enable highlighting of `static`
let g:haskell_backpack = 1                " enable highlighting of backpack keywords
