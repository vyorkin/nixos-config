" stop stretching pinkie
im <silent> <M-i> _
im <silent> <M-r> -

" same but with Cmd on MacVim
im <silent> <D-i> _
im <silent> <D-r> -

" go to last edit location with ,.
nn ,. '.

" jump to line and column by typing '<mark_char>
nn ' `
nn ` '

nn <silent> // :nohlsearch<CR>
" nn <silent> <Esc> :nohlsearch<CR>
