#!/usr/bin/env bash

# Workaround for mpsyt + mpv-0.32.x

shift
mpv --title=$@
