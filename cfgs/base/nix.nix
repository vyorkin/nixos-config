{ pkgs, ... }:

# let
#   nixfmt = import ../../pkgs/custom/nixfmt;
{
  environment.systemPackages = with pkgs; [
    pkgconfig
    nix-prefetch-scripts
    nix-prefetch-github
    nix-du
    nix-index
    nix-serve
    nix-review
    nix-top
    # nixops
    any-nix-shell
    cachix

    nix-bash-completions
    nix-zsh-completions

    nixfmt

    # see: https://nixos.wiki/wiki/Steam#steam-run
    steam-run
    # steam-run-native
  ];
}
