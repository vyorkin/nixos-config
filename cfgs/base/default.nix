{ ... }:

{
  imports = [
    ./tools.nix
    ./nix.nix
    ./generators.nix
  ];
}
