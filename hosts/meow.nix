{ ... }:

{
  imports = [
    ../roles/server.nix
    ../roles/workstation.nix
  ];
}
